package com.automationpractice;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.junit.Test;

/**
 * In the case to allow untrusted users to supply HTML for output on your website
 * (e.g. as comment submission).
 *
 * @author Nick G.
 */
public class SanatizeHtmlTest {

    @Test
    public void sanatizeUnsafeHtml()
    {
        String unsafeHtml =
                "<p><a href='http://example.com/' onclick='stealCookies()'>Link</a></p>";
        String safeHtml = Jsoup.clean(unsafeHtml, Whitelist.basic());

        System.out.println("modified output: " + safeHtml);
    }

}
