# jsoup Java Gradle Example

Example project to demonstrate a JSoup test project in Gradle.

[![pipeline status](https://gitlab.com/gitlab-org/security-products/codequality/badges/master/pipeline.svg)](https://gitlab.com/jnickgo/jsoup-java-gradle/commits/master)

## jsoup

jsoup is a Java library for working with real-world HTML. It provides a very convenient API for extracting and manipulating data, using the best of DOM, CSS, and jquery-like methods.

### jsoup Documentation

- [Home Page](https://jsoup.org/)
- [Cookbook](https://jsoup.org/cookbook/)
- [Javadoc](https://jsoup.org/apidocs/overview-summary.html)
    - primary packages are org.jsoup and org.jsoup.nodes

## Getting Started

Installing jsoup is quite straight-forward.  Since this is a Gradle project simply add the following line to your dependencies task in build.gradle

```
    compile 'org.jsoup:jsoup:1.11.3'
```

Or...

You can download the jar from [here](https://jsoup.org/download)